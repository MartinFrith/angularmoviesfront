angular.module('movieApp',["ngRoute"])
.config(function ($routeProvider) {
	$routeProvider
	.when('/', {
			templateUrl: 'partials/main.html',
			controller: 'mainCtrl'
		})
	.when('/copyleft', {
			templateUrl: 'partials/copyleft.html',
		})
	.when('/credits', {
			templateUrl: 'partials/credits.html',
		})
	.when('/premieres', {
			templateUrl: 'partials/premieres/index.html',
			controller: 'premieresCtrl'
		})
	.when('/recent', {
			templateUrl: 'partials/recent/index.html',
			controller: 'recentCtrl'
		})
	.when('/movies', {
			templateUrl: 'partials/movies/index.html',
			controller: 'moviesCtrl'
		})
	.when('/movies/:filter/:value', {
			templateUrl: 'partials/movies/index.html',
			controller: 'moviesCtrl'
		})
	.when('/movie/add', {
			templateUrl: 'partials/movies/create.html',
			controller: 'movieAddCtrl'
		})
	.when('/movie/:id', {
			templateUrl: 'partials/movies/entry.html',
			controller: 'movieCtrl'
		})
	.when('/movie/edit/:id', {
			templateUrl: 'partials/movies/edit.html',
			controller: 'movieCtrl'
		})

	.otherwise({
			redirectTo: '/'
		});
});

// Checks for nav click events and sets its elements active status
$(document).on('click','.navbar a,.nav a', function(){
	$(this).parent().parent().find('li').removeClass('active');
	$(this).parent().addClass('active');
});

$(function(){
	$('a').tooltip();
});