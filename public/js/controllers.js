var utils = {
  merge : function(original,settings){
    var collection = original;
    for(var i in settings){
      for(var j in original){
        if(original[j].name == settings[i].name){
          collection[j].value = settings[i].value;
        }
      }
    }
    return collection;
  }
};
var endpoint = 'http://localhost:3001/api/v1';
var genres = [{
  name: 'Action',
  value: false
}, {
  name: 'Adventure',
  value: false
}, {
  name: 'Drama',
  value: false
}, {
  name: 'Comedy',
  value: false
}, {
  name: 'Sci-Fi',
  value: false
}, {
  name: 'Mistery',
  value: false
}];

angular.module('movieApp')
 .controller('moviesCtrl', ['$scope', '$routeParams', '$http', function ($scope, $routeParams, $http) {

	var url = endpoint + '/movies';

	if($routeParams.filter && $routeParams.value){
		url+= '/' + $routeParams.filter + '/' + $routeParams.value;
	} 

	$http.get(url).
        success(function(data) {
        	$scope.movies = data;
		});

	$scope.sendEndpoint = function(url) {
		this.sendRequest(endpoint + url);
	};

	$scope.sendRequest = function(url) {
		$http.get(url)
			.success(function(data) {
				$scope.movies = data;
			});
	};

	$scope.search = '';

	$scope.searchMov = function() {
		$http.get( endpoint + '/movies/keyword/' + $scope.search)
			.success(function(data) {
				$scope.movies = data;
			});
	};

	$scope.searchByGenre = function() {
		var form = document.getElementById("genreFilter");
		var elem = form.elements;
		var select = elem[0];
		var index = select.selectedIndex;
		var genre = select[index].value;
		$http.get( endpoint + '/movies/genre/' + genre)
			.success(function(data) {
				$scope.movies = data;
			});
	};
}])
.controller('mainCtrl', ['$scope', '$http', function ($scope, $http) {
	$http.get( endpoint + '/movies/order/title/asc')
		.success(function(data) {
			$scope.movies = data;
		});
}])
.controller('premieresCtrl', ['$scope', '$http', function ($scope, $http) {
	$http.get( endpoint + '/movies/order/title/asc')
		.success(function(data) {
			$scope.movies = data;
		});
}])
.controller('recentCtrl', ['$scope', '$http', function ($scope, $http) {
	$http.get( endpoint + '/movies/order/release/-')
		.success(function(data) {
			$scope.movies = data;
		});
}])
.controller('movieCtrl', ['$scope', '$routeParams', '$http', function ($scope,$routeParams,$http) {
	$http.get( endpoint + '/movie/'+$routeParams.id)
		.success(function(data) {
			$scope.movie = data;
      $scope.movie.release = new Date(data.release)
			$scope.genres = genres;
			if(data.genre) $scope.genres = utils.merge(genres,angular.fromJson(data.genre));
			$scope.movie.genre = $.grep($scope.genres, function(n){ return (n.value); });
		});
    $scope.submit = function() {
    	this.movie.rated = document.getElementById('slider-rated').value;
    	this.movie.genre = angular.toJson($.grep($scope.genres, function(n){ return (n.value); }));
    	$http.put( endpoint + '/movie/'+this.movie.id, this.movie)
    		.success(function(){
    			console.log("movie updated")
    		});
    };
}])
.controller('movieAddCtrl',['$scope', '$http', function ($scope, $http) {
    $scope.newMovie = {};
    $scope.genres = genres;
    $scope.submit = function() {
    	//genres = $.grep(genres, function(n){ return (n.value); });
    	this.newMovie.genre = angular.toJson($scope.genres);
    	$http.post( endpoint + '/movie', this.newMovie)
    		.success(function(){
    			console.log("movie added")
    		});
    };
}])
.controller('RangeCtrl', function ($scope) {
    var num = 0.0;
    $scope.qty = new Quantity(6);
    $scope.num = num;
});

function Quantity(numOfPcs) {
    var qty = numOfPcs;
    var rated = numOfPcs / 1;

    this.__defineGetter__("qty", function () {
        return qty;
    });

    this.__defineSetter__("qty", function (val) {        
        val = parseInt(val);
        qty = val;
        rated = val / 1;
    });

    this.__defineGetter__("rated", function () {
        return rated;
    });

    this.__defineSetter__("rated", function (val) {
        rated = val;
        qty = val * 1;
    });
}