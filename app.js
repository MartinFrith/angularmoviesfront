var express = require('express'),
app = express(),
fs = require("fs"),
url = require('url'),
bodyParser = require('body-parser'),
http = require('http'),
request = require('request'),
port = 3000;

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(express.static(__dirname +'/public'));  
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/public/index.html');
});

app.listen(port);
console.log("The website is running on port: "+port);